﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ShortLinkAPI.Migrations
{
    public partial class database_change : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Links",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    LinkOriginal = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LinkEncurtado = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DataCriacao = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DataModificacao = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Visitados = table.Column<int>(type: "int", nullable: false),
                    Status = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Links", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "Links",
                columns: new[] { "Id", "DataCriacao", "DataModificacao", "LinkEncurtado", "LinkOriginal", "Status", "Visitados" },
                values: new object[] { 1, new DateTime(2022, 4, 4, 16, 50, 35, 219, DateTimeKind.Local).AddTicks(2501), new DateTime(2022, 4, 4, 16, 50, 35, 220, DateTimeKind.Local).AddTicks(1803), "http://teste.com/teste", "http://google.com.br", false, 0 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Links");
        }
    }
}
