﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ShortLinkAPI.Data
{
    public class Link
    {
        [Key]
        public int Id { get; set; }
        public string LinkOriginal { get; set; }
        public string LinkEncurtado { get; set; }
        public DateTime DataCriacao { get; set; }
        public DateTime DataModificacao { get; set; }
        public int Visitados { get; set; }
        public bool Status { get; set; }
    }
}
