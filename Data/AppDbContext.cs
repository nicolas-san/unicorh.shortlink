﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShortLinkAPI.Data
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
        }
        public DbSet<Link> Links { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Link>()
                .HasData(
                        new Link { Id = 1, LinkOriginal = "http://google.com.br", LinkEncurtado = "http://teste.com/teste", DataCriacao = DateTime.Now, DataModificacao = DateTime.Now}
                );
        }
    }
}
