﻿using Microsoft.AspNetCore.Mvc;
using ShortLinkAPI.Data;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ShortLinkAPI.IService
{
    public interface IGenericService<S>
    {
        List<S> GetLink();
        string GetUrl(string url);   
        string PostLink(Link link);
        Task<S> PutLink(Link link);
        Task<bool> DeleteLink(int id);
    }
}
