﻿using Microsoft.AspNetCore.Mvc;
using ShortLinkAPI.Data;
using ShortLinkAPI.IService;

namespace ShortLinkAPI.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class LinkController : GenericController<Link>
    {
        public LinkController(IGenericService<Link> genericService) : base(genericService) { }
    }
}
