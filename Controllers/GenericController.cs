﻿using Microsoft.AspNetCore.Mvc;
using ShortLinkAPI.Data;
using ShortLinkAPI.IService;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace ShortLinkAPI.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class GenericController<S> : Controller where S : class
    {
        private readonly IGenericService<S> _genericService;

        public GenericController(IGenericService<S> genericService)
        {
            _genericService = genericService;
        }
        [HttpGet]
        [Route("Link")]
        public IActionResult GetLink()
        {
            var getLin = _genericService.GetLink();
            return Ok(getLin);
        }

        [HttpGet("{url}")]
        public ActionResult<string> GetUrl(string url)
        {
            var retGetLink2 = _genericService.GetUrl(url);
            string retorno = retGetLink2.ToString();           
            return Redirect(retorno); 
        }

        [HttpPost]
        [Route("Link")]
        public IActionResult PostLink([FromBody] Link link)
        {
            var postRet = _genericService.PostLink(link);
            string retorno = postRet;
            if (postRet == null)
            {
                return Content("Erro!");  
            }
            else
            {
                return Ok(retorno);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteLink(int id)
        {
            var deleteRet = await _genericService.DeleteLink(id);
            if (deleteRet)
            {
                return Ok();
            }
            else
            {
                return NotFound();
            }
        }

        [HttpPut]
        public async Task<IActionResult> PutLink(Link link)
        {
            var retPut = await _genericService.PutLink(link);
            if (retPut == null)
            {
                return null;
            }
            else
            {
                return Ok(retPut);
            }
        }
    }
}