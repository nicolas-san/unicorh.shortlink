﻿using ShortLinkAPI.IService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ShortLinkAPI.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;

namespace ShortLinkAPI.Service
{
    public class LinkService : IGenericService<Link>
    {
        private readonly AppDbContext _context;

        public LinkService(AppDbContext context)
        {
            _context = context;
        }

        public List<Link> GetLink()
        {
            return _context.Links.ToList();
        }

        public string GetUrl(string url)
        {
            var codigoOriginal = _context.Links.Where(u => u.LinkOriginal == url).FirstOrDefault();
            var code = _context.Links.Where(u => u.LinkEncurtado == url).FirstOrDefault();
            if (code != null)
            {
                var count = _context.Links.Where(u => u.Id == code.Id).FirstOrDefault();
                count.Visitados++;
                _context.Update(count);

                try
                {
                    _context.SaveChanges();                         
                    return code.LinkOriginal;
                }
                catch (DbUpdateConcurrencyException e)
                {
                    if (!UrlExists(url))
                    {
                        return "Url não existe.";
                    }
                    else
                    {
                        throw new Exception(e.Message);
                    }
                }
            }
            else if(codigoOriginal!=null)
            {                
                return $"https://{codigoOriginal.LinkOriginal}";
            }
            else
            {
                return "Url não existe.";
            }
        }

        public string PostLink(Link link)
        {
            var code = GerarToken();
            Link ret = new Link();
            link.DataCriacao = DateTime.Now;
            ret = _context.Links.Where(i => i.LinkOriginal == link.LinkOriginal).FirstOrDefault();
            
            if (!(UrlExists(link.LinkOriginal)))
            {
                link.LinkEncurtado = $"{code}";
                _context.Links.Add(link);
                _context.SaveChanges();
            }
            else
            {
                return $"Link já existe: {ret.LinkEncurtado}";
            }

            return link.LinkEncurtado;
        }

        public async Task<Link> PutLink(Link link)
        {
            var retId = await _context.Links.FirstOrDefaultAsync(x => x.Id == link.Id);
            if (retId == null)
            {
                return null;
            }

            try
            {
                link.DataModificacao = DateTime.Now;
                _context.Entry(retId).CurrentValues.SetValues(link);

                await _context.SaveChangesAsync();
                return link;
            }
            catch (DbUpdateConcurrencyException e)
            {
                throw new Exception(e.Message);
            }
        }

        public async Task<bool> DeleteLink(int id)
        {

            var link = _context.Links.Find(id);
            if (link != null)
            {
                _context.Links.Remove(link);
                await _context.SaveChangesAsync();
                return true;
            }
            else
            {
                return false;
            }
        }

        private string GerarToken(int length = 6)
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var stringChars = new char[length];
            var random = new Random();

            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }

            return new String(stringChars);

        }
        private bool LinkExists(int id)
        {
            return _context.Links.Any(l => l.Id == id);
        }
        private bool UrlExists(string url)
        {
            Link result = new Link();
            try
            {
                result = _context.Links.Where(i => i.LinkOriginal == url).FirstOrDefault();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            if (result == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}